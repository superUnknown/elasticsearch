# #README# #
### Kamil Budka, 217134 ###

## Readme z opisem zadania na egzamin (Elasticsearch). ##

Dane ( aplikacja, instancja Kibana, Elasticsearch, Logstash ) nie zostały dodane do repo [ ~180MB ].



### Użyte narzędzia ###

* Kibana (dashboard pozwalający przeprowadzić wizualizację logów)
* Elasticsearch (engine wyszukiwarki, narzędzie gromadzące, grupujące i filtrujące logi, pozwala nimi zarządzać)
* Logstash (narzędzie pozwalające parsować logi)
* nginx (serwer aplikacji, na którym został postawiony dashboard Kibana)
* Grok Debugger (narzędzie pozwalające tworzenie parsera logów dla Logstash-a oraz ich testowanie w czasie rzeczywistym)
* log4j (logger dla aplikacji tworzonych w Javie)

### Do czego wykorzystałem Loggera? ###
Podczas tworzenia aplikacji oraz jej utrzymywania pomocne jest gromadzenie logów (informacji o błędach, śledzenia przebiegu aplikacji, testowania cache-y). Do aplikacji webowej (JEE) podpiąłem narządzie log4j. Następnie logowałem przepływ informacji ( dodawanie użytkowników, pobieranie danych z bazy, updaty bazy, błędy itd.).
Utworzyłem parser dla logów i podpiąłem do Logstash-a.

Wynik, który otrzymałem:

![screen.png](https://bitbucket.org/repo/Ey4Rbb/images/3698617762-screen.png)

## Praca nad zadaniem ##

### Podpięcie log4j do aplikacji ###

Przykładowe wywołanie logiki 

```
#!java
private static final Logger LOG = Logger.getLogger(PlayerMapper.class);
...
public PlayerDTO entity2dto(Player player) {
		PlayerDTO playerDTO = new PlayerDTO();

		playerDTO.setPlr_id(player.getPlr_id());
		playerDTO.setPlayerName(player.getPlayerName());
		playerDTO.setUsr_id(player.getUser().getUsr_id());
		playerDTO.setDate_registration(DataParser.parseDate(player.getDate_registration(), "DATE"));
		PokerRoomDTO pokerRoomDTO = pokerRoomMapper.entity2dto(player.getPokerRoom());
		playerDTO.setPokerRoom(pokerRoomDTO);

		LOG.info("Mappowanie playera " + player.getPlayerName() + " na DTO."); // logowanie czynności
		
		return playerDTO;
	}
...
```
i wywołanie loggera parsowanego poniższym wzorcem:

```
log4j.appender.file.layout.ConversionPattern=%d{HH:mm:ss dd-MM-yyyy}|[%p]|%C|%m%n
```
dał logi tego formatu:
```
19:52:38 04-01-2015|[INFO]|com.theend.holdemTracker.mappers.PlayerMapper|Mappowanie playera gracz1111 na DTO.
```
### Grok Debugger-em stworzyłem parsera dla logów ###
```
TIME (\d+:\d+:\d+ \d+-\d+-\d+) => 19:52:38 04-01-2015
TYPE (\S+) => INFO
CLASS (\S+) => com.theend.holdemTracker.mappers.PlayerMapper
MSG (.*\|*) => Mappowanie playera gracz1111 na DTO
```

Interpretacja przez Logstash-a:

```
#!JSON

{
       "message" => "19:52:38 04-01-2015|[INFO]|com.theend.holdemTracker.mappers.PlayerMapper|Mappowanie playera gracz1111 na DTO.",
      "@version" => "1",
    "@timestamp" => "2015-01-04T18:52:38.301Z",
          "type" => "Holdem Tracker",
          "host" => "TheEnd.local",
          "path" => "/Applications/Eclipse/Tomcat/logs/holdemTracker.log",
          "time" => "19:52:38 04-01-2015",
       "logType" => "INFO",
         "class" => "com.theend.holdemTracker.mappers.PlayerMapper",
           "msg" => "Mappowanie playera gracz1111 na DTO."
}
```


### Konfiguracja logów w Logstashu ###

```
#!javascript
// konfiguracja strumienia wejściowego (ścieżka do logów, nadanie typu dla strumienia)
input {
  file {
    path => "/Applications/Eclipse/Tomcat/logs/holdemTracker.log"
    type => "Holdem Tracker"
    start_position => "beginning"
  }
}

// filtr korzystający z typu "Holdem Tracker" (wykorzystuje utworzony wzorzec i parsuje)
filter {
  if [type] == "Holdem Tracker" {
    grok {
      patterns_dir => "./patterns"
      match => { "message" => "%{TIME:time}\|\[%{TYPE:logType}\]\|%{CLASS:class}\|%{MSG:msg}" }
    }
  }
}
// strumień wyjściowy ( poprzez logstach-a do elasticsearch )
output {
  elasticsearch { host => localhost }
  stdout { codec => rubydebug }
}
```


### Konfiguracja Kibana ###

* Filtry i obiekty Query

Problem był taki, że log4j logował również czynności których nie chciałem wyświetlać ( wywołania kierowane przez framework Spring MVC i Hibernate w aplikacji). Logstash ściągał logi również z innych miejsc.

Dodałem filtry, aby wyświetlały się tylko logi z logiki biznesowej aplikacji, pozostałe są pogrupowane na diagramie kołowym.

* Utworzyłem panel grupujący podsumowanie logów (z podziałem na błędy i inne info)
* Utworzyłem tablicę ostatnich logów dla aplikacji.